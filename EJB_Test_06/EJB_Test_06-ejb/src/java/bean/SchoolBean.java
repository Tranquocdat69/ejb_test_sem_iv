/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import entities.TblSchool;
import java.util.List;
import javax.ejb.Stateless;
import school.implement.SchoolDAOImplement;

/**
 *
 * @author tranq
 */
@Stateless
public class SchoolBean implements SchoolBeanLocal {

    @Override
    public List<TblSchool> listOfSchool() {
        return new SchoolDAOImplement().listOfSchool();
    }

    @Override
    public boolean insertSchool(TblSchool school) {
        return new SchoolDAOImplement().insertSchool(school);
    }

    @Override
    public List<TblSchool> searchSchoolByName(String schoolName) {
        return new SchoolDAOImplement().searchSchoolByName(schoolName);
    }
    
    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
}
