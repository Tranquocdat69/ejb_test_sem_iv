/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import entities.TblSchool;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author tranq
 */
@Local
public interface SchoolBeanLocal {
    public List<TblSchool> listOfSchool();
    
    public boolean insertSchool(TblSchool school);
    
    public List<TblSchool> searchSchoolByName(String schoolName);
}
