/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package school.dao;

import entities.TblSchool;
import java.util.List;

/**
 *
 * @author tranq
 */
public interface SchoolDAO {
    public List<TblSchool> listOfSchool();
    
    public boolean insertSchool(TblSchool school);
    
    public List<TblSchool> searchSchoolByName(String schoolName);
}
