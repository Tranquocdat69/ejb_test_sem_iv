/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package school.implement;

import entities.TblSchool;
import java.util.List;
import org.hibernate.Session;
import school.dao.SchoolDAO;
import util.HibernateUtil;

/**
 *
 * @author tranq
 */
public class SchoolDAOImplement implements SchoolDAO{

    @Override
    public List<TblSchool> listOfSchool() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            session.beginTransaction();
            List list = session.createQuery("from TblSchool where status = 1")
                    .list();
            session.getTransaction().commit();
            return list;
        }catch(Exception e){
            e.printStackTrace();
            session.getTransaction().rollback();
        }finally{
            session.close();
        }
        return null;
    }

    @Override
    public boolean insertSchool(TblSchool school) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            session.beginTransaction();
            session.save(school);
            session.getTransaction().commit();
            return true;
        }catch(Exception e){
            e.printStackTrace();
            session.getTransaction().rollback();
        }finally{
            session.close();
        }
        return false;
    }

    @Override
    public List<TblSchool> searchSchoolByName(String schoolName) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            session.beginTransaction();
            if (schoolName == null || schoolName.length() == 0) {
                schoolName = "%";
            }else{
                schoolName = "%"+schoolName+"%";
            }
            List list = session.createQuery("from TblSchool where schoolName like :schoolName")
                    .setParameter("schoolName", schoolName)
                    .list();
            session.getTransaction().commit();
            return list;
        }catch(Exception e){
            e.printStackTrace();
            session.getTransaction().rollback();
        }finally{
            session.close();
        }
        return null;
    }
    public static void main(String[] args) {
        System.out.println(new SchoolDAOImplement().listOfSchool().size());
    }
}
