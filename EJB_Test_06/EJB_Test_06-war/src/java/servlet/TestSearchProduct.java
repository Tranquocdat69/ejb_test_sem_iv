/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import bean.SchoolBeanLocal;
import entities.TblSchool;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author tranq
 */
@WebServlet(name = "TestSearchProduct", urlPatterns = {"/TestSearchProduct"})
public class TestSearchProduct extends HttpServlet {

    @EJB
    private SchoolBeanLocal schoolBean;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet TestSearchProduct</title>");            
            out.println("</head>");
            out.println("<body>");
            
            String baseTable = "<center><table border=1>"
                            + "<thead>"
                            + "<th>School Name</th>"
                            + "<th>Address</th>"
                            + "<th>Training Field</th>"
                            + "<th>Area</th>"
                            + "<th>TotalStudents</th>"
                            + "<th>Status</th>"
                            + "</thead>"
                            +"<tbody>";
            String bodyTable = "";
            
            List<TblSchool> listOfSchool = new ArrayList<>();
            
            if (request.getParameter("keyword") == null) {
                listOfSchool = schoolBean.listOfSchool();
                out.println("<center><h1>List of schools with status equalling 1: </h1></center>");
            }else{
                 String rawKeyword = request.getParameter("keyword");
                 String encodedKeyword = URLEncoder.encode(rawKeyword,"ISO-8859-1");
                 String decodedKeyword = URLDecoder.decode(encodedKeyword,"UTF-8");
            
                listOfSchool = schoolBean.searchSchoolByName(decodedKeyword);
                out.println("<center><h1>Result searched by school name: "+decodedKeyword+" </h1></center>");
                out.println("<center><h2>Found: "+listOfSchool.size()+" results </h2></center>");
            }
                for (TblSchool tblSchool : listOfSchool) {
                    bodyTable += "<tr>"
                            + "<td>"+tblSchool.getSchoolName()+"</td>"
                            + "<td>"+tblSchool.getAddress()+"</td>"
                            + "<td>"+tblSchool.getTrainingField()+"</td>"
                            + "<td>"+tblSchool.getArea()+"</td>"
                            + "<td>"+tblSchool.getTotalStudents()+"</td>"
                            + "<td>"+((tblSchool.getStatus()) ? "Hoạt động" : "Giải thể" )+"</td>"
                            + "</tr>";
                }
            
                String table = baseTable + bodyTable+ "</tbody></table></center>";;
                out.print(table);
            
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
