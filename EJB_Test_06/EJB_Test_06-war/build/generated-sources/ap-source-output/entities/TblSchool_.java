package entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-10-17T13:49:20")
@StaticMetamodel(TblSchool.class)
public class TblSchool_ { 

    public static volatile SingularAttribute<TblSchool, Double> area;
    public static volatile SingularAttribute<TblSchool, String> trainingField;
    public static volatile SingularAttribute<TblSchool, String> address;
    public static volatile SingularAttribute<TblSchool, String> schoolId;
    public static volatile SingularAttribute<TblSchool, Integer> totalStudents;
    public static volatile SingularAttribute<TblSchool, String> schoolName;
    public static volatile SingularAttribute<TblSchool, Boolean> status;

}