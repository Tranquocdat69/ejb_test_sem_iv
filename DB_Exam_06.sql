create database DB_EJB_Exam06
go

use DB_EJB_Exam06
go

create table TblSchool(
	SchoolId varchar(10) not null primary key,
	SchoolName nvarchar(200),
	Address nvarchar(200),
	TrainingField nvarchar(200),
	Area float,
	TotalStudents int,
	Status bit
)
go

update TblSchool set Status = 0 where SchoolId = 'HVKTQS01';
go

select * from TblSchool
go